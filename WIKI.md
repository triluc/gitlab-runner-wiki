# Custom gitlab runners

**DRAFT - INTENDED AS A WIKI PAGE IN http://hub.eyeo.com/projects/ops/wiki/**

This page aims to guide through the process of creating and maintaining custom [gitlab runner](https://docs.gitlab.com/runner/) instances per project (i.e. repository / team).

### Trivia
The introduction of self hosted gitlab runners was motivated by three major reasons:

 1. Control about what hardware is available for a given task
 2. Better control about our confidential data / access keys
 3. Independence from GitLab's [CI pipeline minutes](https://about.gitlab.com/pricing/)

All of the above should be taken into account, when considering the creation of a custom gitlab runner.

## Prerequisites
 * [Python](https://www.python.org/download/releases/2.7/) v2.7
 * [Ansible](https://pypi.org/project/ansible/) >=v2.7.1
 * [yamllint](https://yamllint.readthedocs.io/en/v1.14.0/) >=v1.14.0

## Optional (for development and testing)
 * [Vagrant](https://www.vagrantup.com/) >=v2.0.0
 * [VirtualBox](https://www.virtualbox.org/wiki/Downloads) >=v5.0
 
## Step by step creation of your own custom gitlab runner
(through the example of [ansible-role-adblockplus-builder](https://gitlab.com/eyeo/devops/ansible-role-adblockplus-builder) in Jan' 19, provisioned as pure [shell executors](https://docs.gitlab.com/runner/executors/shell.html))

1. Accumulate a list of needed software packages (currently for debian) which are needed in order to run all your desired tasks
2. Copy or fork our [ansible-role-example](https://gitlab.com/eyeo/devops/examples/ansible-role-example) to `ansible-role-adblockplus-builder`
3. In `Vagrantfile`, rename the variable `config.vm.hostname` to something suitable for your project (`adblockplus-builder-test` in our example) and also adjust the virtual machine's name to something suitable for your project (`adblockplus-builder-test` in our example)
3. Add the submodule [ansible-role-gitlab-runner](https://gitlab.com/eyeo/devops/ansible-role-gitlab-runner) as `gitlab-runner` into the folder `roles/`
4. In `./playbook.yml`, add the following lines to import the `gitlab-runner ` setup and registration routines:

```yaml
    # https://docs.ansible.com/ansible/latest/import_role_module.html
    - import_role:
        name: "gitlab-runner"
```

5. In `tasks/main.yml`, add all necessary prerequisites for the final build.
In case of `ansible-role-adblockplus-builder`, building [Adblock Plus for Chrome source
code](https://gitlab.com/eyeo/adblockplus/adblockpluschrome), that means
all necessary software packages that have to be installed.
(See e.g. [Ansible's `apt` module](https://docs.ansible.com/ansible/latest/modules/apt_module.html) for more
information.)
The resulting `tasks/main.yml` for `ansible-role-adblockplus-builder` additionally adds the repository for Node.js: [tasks/main.yml@d415dcf6](https://gitlab.com/eyeo/devops/ansible-role-adblockplus-builder/blob/d415dcf6/tasks/main.yml)
6. Using Vagrant, verify that all software packages are installed successfully and no other error occurs:
```shell
$ vagrant up adblockplus-builder-test
```
7. If you want to actually test the registration and / or if your list of
necessary software is complete, you are now able to register a virtual
machine as an actually specific runner for a project and run CI jobs on it.

The idea is basically that you obtain a new runner registration token,
from the repository/project that provides the final, most specific, building
block to your desired output. In case of "ansible-role-adblockplus-builder",
that is the "adblockpluschrome" repository.

Once you're in the aforementioned repository, open "Settings" -> "CI / CD"
-> "Runners" -> "Specific Runners" and under "Set up a specific Runner manually"
define a new registration token that you will then enter into your virtual
machine.

In that repository you also then add a `.gitlab-ci.yml` file which defines
what exactly gets run by the Runner.

For more information, please consult
[ansible-role-gitlab-runner](https://gitlab.com/eyeo/devops/ansible-role-gitlab-runner/blob/master/README.md)'s
README and GitLab's [documentation for CI](https://docs.gitlab.com/ee/ci/)
in order to set up a suitable test environment.

## Involving [#ops](https://gitlab.com/eyeo/devops)

To actually rollout your custom runner, please follow these steps:

**ADJUST EVERYTHING BELOW ONCE ansible-playbooks IS RELEASED**

8. Clone [ansible-playbooks](https://gitlab.com/eyeo/devops/ansible-playbooks)
9. Add your repository as a submodule into `roles/` with a meaningful name, without the preceding `ansible-role-`, in our example `adblockplus-builder`
10. Create a new playbook `./provision-<your-runnner>.yml` which imports your new role, as well as the `ansible-role-gitlab-runner`:
```yaml
- name:
    "provision-adblockplus-gitlab-runners"
  hosts:
    "adblockplus-gitlab-runners"
  tasks:
    # https://gitlab.com/eyeo/devops/ansible-role-adblockplus-builder
    - import_role:
        name: "adblockplus-builder"
      tags:
        - "setup"
    # https://gitlab.com/eyeo/devops/ansible-role-gitlab-runner
    - import_role:
        name: "gitlab-runner"
```

11. File a merge request for `ansible-playbooks` including the above changes
12. Address all comments in the review
13. Merge
14. Create a hub-ticket with additional information about the configuration (e.g. hardware or [gitlab-runner registration parameters](https://docs.gitlab.com/runner/commands/#gitlab-runner-register))
15. In the repository where you registered the runner, open "Settings" ->
    "CI / CD" -> "Runners" and click "Disable shared runners" for this project
16. Verify that the next pushed commit triggers a build on your new specific
    Runner
17. Celebrate

## Maintenance
When you remove or add something to you custom gitlab runner, simply file a merge request for your custom gitlab runner (make sure to involve someone from ops), followed by an addtionak merge request for `ansible-playbooks` which updates the submodule to the latest version of your repository.